clear;

src=$1
declare -a srcArray
srcArraySize=0

while true; do
    if [[ ! -z $src ]]; then
        while read -r line; do
            if [[ -f $line ]]; then
                srcArray[srcArraySize]="$line"
                ((srcArraySize++))
            else
                echo -e "\n\n--- ERROR:\n \"$line\" is not a valid file"
                src=""
                break;
            fi
        done < <(sed -e $'s/ \//\\\n\//g'<<<"$src")
        
        if [[ ! -z $src ]]; then
            break;
        fi
    else 
        read -p "Drag in some .doc files: " src
    fi
done

for file in "${srcArray[@]}"; do

    if [[ ! -f $tempFile ]]; then
        textutil -convert txt -extension temp "$file"

        filePath=${file%/*}
        fileBase=${file##*/}
        fileName=${fileBase%.*}
    
        tempFile="$filePath/$fileName.temp"
        newFile="$filePath/$fileName.txt"
    
        if [[ -f $newFile ]]; then
            echo ""
            echo ""
            echo "Error converting $fileBase. $newFile already exists."
        else
                echo -e "\n\n\tConverting $fileBase"
                fold -s -w40 "$tempFile" | iconv -f UTF-8 -t MACROMAN > "$newFile"
        fi

        rm "$tempFile"
    else
        echo "ERROR: Couldn't create necessary temporary files"
    fi
    
    echo -e "\tDone!"
done

